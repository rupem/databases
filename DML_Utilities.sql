SET SERVEROUTPUT ON;

--alter session set nls_date_format = 'DD-MON-YYYY HH24:MI:SS';

CREATE OR REPLACE PACKAGE DML_Utilities IS
    PROCEDURE softDelete(p_inputId NUMBER, p_tablename VARCHAR2);
END DML_Utilities;
/

CREATE OR REPLACE PACKAGE BODY DML_Utilities IS
    PROCEDURE softDelete(p_inputId NUMBER, p_tablename VARCHAR2) AS
        v_cursor NUMBER;
        v_returnRows NUMBER;
        v_statement VARCHAR2(255);
    BEGIN
        --DBMS_OUTPUT.PUT_LINE('gg');
        v_cursor := DBMS_SQL.OPEN_CURSOR;
        v_statement := ' UPDATE ' || p_tablename || ' SET IS_DELETED = 1 WHERE ' || p_tablename || '_ID = ' || p_inputId;
        DBMS_SQL.PARSE(v_cursor, v_statement, DBMS_SQL.V7);
        v_returnRows := DBMS_SQL.EXECUTE(v_cursor);
        --DBMS_OUTPUT.PUT_LINE(v_statement);
    END softDelete;
END DML_Utilities;
/

CREATE OR REPLACE TRIGGER MY_USERS_SOFT_DELETE BEFORE DELETE ON MY_USERS FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.MY_USERS_ID, 'MY_USERS');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER TAGS_SOFT_DELETE BEFORE DELETE ON TAGS FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.TAGS_ID, 'TAGS');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER PEOPLE_SOFT_DELETE BEFORE DELETE ON PEOPLE FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.PEOPLE_ID, 'PEOPLE');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER PHOTOS_SOFT_DELETE BEFORE DELETE ON PHOTOS FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.PHOTOS_ID, 'PHOTOS');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/
--DROP TRIGGER VIDEOS_SOFT_DELETE;
CREATE OR REPLACE TRIGGER VIDEOS_SOFT_DELETE BEFORE DELETE ON VIDEOS FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.VIDEOS_ID, 'VIDEOS');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER PDFS_SOFT_DELETE BEFORE DELETE ON PDFS FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.PDFS_ID, 'PDFS');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER DOCS_SOFT_DELETE BEFORE DELETE ON DOCS FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.DOCS_ID, 'DOCS');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER PHOTO_PEOPLE_SOFT_DELETE BEFORE DELETE ON PHOTO_PEOPLE FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.PHOTO_PEOPLE_ID, 'PHOTO_PEOPLE');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

CREATE OR REPLACE TRIGGER VIDEO_PEOPLE_SOFT_DELETE BEFORE DELETE ON VIDEO_PEOPLE FOR EACH ROW
DECLARE
    PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
    DML_Utilities.softDelete(:OLD.VIDEO_PEOPLE_ID, 'VIDEO_PEOPLE');
    COMMIT;
    raise_application_error(-20000,'Hard Delete is not allowed. Soft Delete Strategy applied.');
END;
/

/*
BEGIN
    --DML_Utilities.softDelete(932, 'TAGS');
    --DELETE FROM MY_USERS WHERE MY_USERS_ID = 180;
END;
*/