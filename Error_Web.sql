SET SERVEROUTPUT ON;

CREATE OR REPLACE PACKAGE stupid_question_select IS
  TYPE resultTable IS RECORD(question QUESTIONS.QUESTION%TYPE, answer QUESTIONS.ANSWER%TYPE);
  FUNCTION get_question_with_id(p_id NUMBER) RETURN resultTable deterministic;
END stupid_question_select;
/

CREATE OR REPLACE PACKAGE BODY stupid_question_select IS
  FUNCTION get_question_with_id(p_id NUMBER) RETURN resultTable deterministic AS
    question resultTable;
  BEGIN
    SELECT DBMS_LOB.SUBSTR(question, 4000, 1) AS question, DBMS_LOB.SUBSTR(answer,4000, 1) AS answer INTO question FROM questions WHERE id = p_id;
    RETURN question;
    EXCEPTION 
            WHEN NO_DATA_FOUND THEN
                raise_application_error (-20021,'ID inexistent');
  END get_question_with_id;
END stupid_question_select;
/
/*
DECLARE
    ourResult stupid_question_select.resultTable;
BEGIN
    ourResult := stupid_question_select.get_question_with_id(6);
    DBMS_OUTPUT.PUT_LINE(ourResult.question);
    DBMS_OUTPUT.PUT_LINE(ourResult.answer);
END; */