SET SERVEROUTPUT ON;
CREATE OR REPLACE PACKAGE AppExceptions is

    invalid_user_id EXCEPTION;
    pragma exception_init(invalid_user_id, -20001);
    invalid_person_id EXCEPTION;
    pragma exception_init(invalid_person_id, -20002);
    invalid_nickname EXCEPTION;
    pragma exception_init(invalid_nickname, -20003);
    invalid_father_id EXCEPTION;
    invalid_input_type EXCEPTION;
    invalid_relation_path EXCEPTION;
    update_actual_user_nickname EXCEPTION; -- nickname-ul pt actual user nu trebuie sa poata fi modificat
    multiple_fathers EXCEPTION; -- vom presupune ca o persoana nu poate avea mai multi tati
    update_flag EXCEPTION; -- exceptia descrisa in exemplul cu bunicii deja existenti si flagul update pe 0
    gods_parent_access EXCEPTION; -- nu se poate accesa parintele DOMNULUI !!
    
END AppExceptions;