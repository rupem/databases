SET SERVEROUTPUT ON;

DROP TABLE TreeTable;
CREATE TABLE TreeTable(Person_ID NUMBER(10,0) NOT NULL, User_ID VARCHAR2(255 BYTE) NOT NULL,
        Nickname VARCHAR2(255 BYTE), Father_ID NUMBER(10,0), PRIMARY KEY(Person_ID), constraint nickname_per_user unique(Nickname, User_ID));
DESCRIBE TreeTable;


BEGIN
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (1,'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0','Taticut',0);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (2,'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0','actualUser'||'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0',1);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (3,'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0','Copilas2',1);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (4,'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0','Copilas1DeActualUser',2);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (5,'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0','Copilas1DeCopilas2',3);
END;

