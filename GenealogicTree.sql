set serveroutput on;
create or replace package FamilyTree is

person_count number(10,0) := 0;

FUNCTION get_person_id(p_nickname VARCHAR2) RETURN NUMBER DETERMINISTIC;
FUNCTION get_person_nickname(p_person_id NUMBER) RETURN VARCHAR2 DETERMINISTIC;
FUNCTION get_parent_id(p_person_id NUMBER) RETURN NUMBER DETERMINISTIC;
FUNCTION get_parent_id(p_nickname VARCHAR2) RETURN NUMBER DETERMINISTIC;
FUNCTION get_user_id(p_person_id NUMBER) RETURN NUMBER DETERMINISTIC;
FUNCTION get_user_id(p_nickname VARCHAR2) RETURN NUMBER DETERMINISTIC;
procedure add_person(p_user_id VARCHAR2,p_nickname VARCHAR2,
       p_relation_path VARCHAR2, p_root_nickname VARCHAR2, p_update NUMBER);
function MaximumKinshipOfAsset(p_my_users_id varchar2,p_asset_id number,p_file_type varchar2) return NUMBER;

/*relation path va fi un string de forma :
( ((X)^*)|((X)^*)N ) adica N daca exista,este neaparat la final
unde X apartine: F-father 
                      S-son
                      N-new son <-care deocamdata vom considera ca poate fi doar la final 
  Parcurgerea se face incepand de la p_root_nickname.
  De exemplu, daca vrem sa adaugam 
    1)parinti
  vom specifica ca root_nickname "Self" sau care ar fi cuvantul rezervat pentru utilizatorul efectiv 
  si path-ul : F
   2) frate(eventual include si nevasta lui): FN
   3) var(eventual include si nevasta lui): 
        deocamdata,pt ca nu stim copilul carui unchi va fi, nu putem aduga un var decat daca i-am adaugat inainte tatal, sau bunicul, sau nepotul.
          De ex daca i-am adaugat tatal inainte setam tatal ca p_root_nickname si path va fi simplu N.
   4)bunici :FF
      Daca bunicii deja exista si flagul de p_update e setat pe false se va ridica o eroare.Daca exista si flag-ul e pe true se va suprascrie nickname-ul
    
    Singurul moment cand nu va ridica eroare va fi atunci cand nodul efectiv la care a ajuns este AE.Atunci  Mai multe explicatii la functia 
    update_parent(care va trebui si ap elata la sfarsit).
    OBS:Asta se va intampla doar cand ultimul caracter din path este P.
    OBS: Nu are voie ca ultimul caracter din path sa fie S
    5)Daca incercam sa accesam parent-ul lui AE se va ridica o eroare.
*/
function find_path(p_nickname_root VARCHAR2,p_nickname_result VARCHAR2) return VARCHAR2 DETERMINISTIC;
--procedure update_parent(p_nickname_new VARCHAR2);
/*
p_nickname_new : noul nod adaugat ca si parinte in locul AE fata de o persoana;
Toate persoanele (mai putin cea proaspat adaugata) care au ca parinte AE trebuie sa aiba ca parinte aceasta persoana proaspat adaugata.
Persoanei proaspat adaugate i se va adauga ca parinte AE.
Motivatie:
dupa cum se va observa in poza, atunci cand adaugam bunicii, trebuie sa-i setam ca si parent si pt unchi.
*/
function getKinshipDegree(p_user_id varchar2,p_person_id number) return integer deterministic;

type children_array is varray(100) of TreeTable%rowtype;
function get_all_children(p_root_nickname VARCHAR2, p_user_id VARCHAR2) return varchar2 deterministic;

procedure update_parent(p_root_id number, p_new_parent_id number);
end FamilyTree;
/

CREATE OR REPLACE PACKAGE BODY FamilyTree IS

    FUNCTION get_person_id(p_nickname VARCHAR2) RETURN NUMBER DETERMINISTIC AS
    v_person_id NUMBER:=-1;
    v_count number:=0;
    BEGIN
        select count(*) into v_count from treetable where upper(p_nickname) = upper(Nickname);
        if v_count>0 then
          sys.dbms_output.put_line('am gasit ceva');
          SELECT Person_ID INTO v_person_id FROM TreeTable WHERE upper(p_nickname) = upper(Nickname);
          sys.dbms_output.put_line('am gasit ceva: '||v_person_id);
          RETURN v_person_id;
          sys.dbms_output.put_line('am mai gasit ceva: '||v_person_id);
        end if;
        return -1;
    END get_person_id;
    
    function MaximumKinshipOfAsset(p_my_users_id varchar2,p_asset_id number,p_file_type varchar2) return NUMBER as
    v_people varchar2(1000);
    v_max_kinship number:=-1;
    it number:=-1;
    v_count number:=1;
    v_prev number:=1;
    v_person varchar2(100);
    v_person_id number;
    v_degree number:=-1;
    v_my_users_id number:=-1;
    begin
    v_my_users_id:=FAMILYTREE.get_person_id('actualUser'||'eu-west-1:9d5ef8fc-b204-489a-9de4-de2415e6eaf0');  
    sys.dbms_output.put_line('users_id: '||v_my_users_id);
    v_people:=TAGS_PERSONS_UTILS.getPeople(p_my_users_id,p_asset_id,p_file_type);
--    v_people:=reverse(v_people);
    sys.dbms_output.put_line('inceput=========================');
    sys.dbms_output.put_line(v_people);
    it:=instr(v_people,'SMRP',1,v_count);
    sys.dbms_output.put_line(it);
    
    while(it<>0) loop
      sys.dbms_output.put_line('it : '||it);
      v_person:=substr(v_people,v_prev,it-v_prev);
      sys.dbms_output.put_line('person: '||v_person);
      v_person_id:=familytree.get_person_id(v_person);
      sys.dbms_output.put_line('id: '||v_person_id);
      if v_person_id =-1 then
        sys.dbms_output.put_line('NU E IN FAMILY TREE');
      else 
        sys.dbms_output.put_line('este!');
        v_degree:=FAMILYTREE.GETKINSHIPDEGREE(p_my_users_id,v_person_id);
        sys.dbms_output.put_line('grad: '||v_degree);
        if v_degree>v_max_kinship then
          v_max_kinship:=v_degree;
        end if;
          
      end if;
      v_prev:=it+4;
      v_count:=v_count+1;
      it:=instr(v_people,'SMRP',1,v_count);
    end loop;
    sys.dbms_output.put_line('max: '||v_max_kinship);
--    
--    
    
      
    return v_max_kinship;
    end MaximumKinshipOfAsset;
    
    FUNCTION get_person_nickname(p_person_id NUMBER) RETURN VARCHAR2 DETERMINISTIC AS
    v_person_nickname VARCHAR2(255 BYTE);
    BEGIN
        SELECT Nickname INTO v_person_nickname FROM TreeTable WHERE p_person_id = Person_ID;
        RETURN v_person_nickname;
    END get_person_nickname;
    
    FUNCTION get_parent_id(p_person_id NUMBER) return NUMBER DETERMINISTIC AS
    v_parent_id NUMBER;
    BEGIN
        SELECT Father_ID INTO v_parent_id FROM TreeTable WHERE p_person_id = Person_ID;
        RETURN v_parent_id;
    END get_parent_id;
    
    FUNCTION get_parent_id(p_nickname VARCHAR2) return NUMBER DETERMINISTIC AS
    v_parent_id NUMBER;
    BEGIN
        SELECT Father_ID INTO v_parent_id FROM TreeTable WHERE p_nickname = Nickname;
        RETURN v_parent_id;
    END get_parent_id;
    
    FUNCTION get_user_id(p_person_id NUMBER) RETURN NUMBER DETERMINISTIC AS
    v_user_id NUMBER;
    BEGIN
        SELECT User_ID INTO v_user_id FROM TreeTable WHERE p_person_id = Person_ID;
        RETURN v_user_id;
    END get_user_id;
    
    FUNCTION get_user_id(p_nickname VARCHAR2) RETURN NUMBER DETERMINISTIC AS
    v_user_id NUMBER;
    BEGIN
        SELECT User_ID INTO v_user_id FROM TreeTable WHERE p_nickname = Nickname;
        RETURN v_user_id;
    END get_user_id;
    
    function find_path(p_nickname_root VARCHAR2,p_nickname_result VARCHAR2) return VARCHAR2 DETERMINISTIC as
    
    v_current_parent_of_root number;
    v_current_parent_of_result number;
    v_res1 varchar2(1000);
    v_res2 varchar2(1000);
    
    begin
    --SYS.DBMS_OUTPUT.PUT_LINE('a');
      v_current_parent_of_root :=get_person_id(p_nickname_root);
      --SYS.DBMS_OUTPUT.PUT_LINE('v_current_parent_of_root: '||v_current_parent_of_root);
      while (v_current_parent_of_root!=0) loop
        v_current_parent_of_result:= get_person_id(p_nickname_result);
        v_res2:=null;
        --SYS.DBMS_OUTPUT.PUT_LINE('v_current_parent_of_result: '||v_current_parent_of_result);
        while(v_current_parent_of_result!=0) loop
          
          if v_current_parent_of_result=v_current_parent_of_root then
            return v_res1||v_res2;
          end if;
          v_current_parent_of_result:=get_parent_id(v_current_parent_of_result);
          v_res2:=v_res2||'S';
        end loop;
        v_current_parent_of_root:=get_parent_id(v_current_parent_of_root);
        v_res1:=v_res1||'F';
      end loop;
    return v_res1||v_res2;
    end find_path;
    
    function getKinshipDegree(p_user_id varchar2,p_person_id number) return integer deterministic as
    v_nickname1 varchar2(100);
    v_nickname2 varchar2(100);
    v_result varchar2(100);
    v_actual_user_id number(10,0);
    begin
      v_actual_user_id:=get_person_id('actualUser'||p_user_id);
      SYS.DBMS_OUTPUT.PUT_LINE(v_actual_user_id);
      if v_actual_user_id=p_person_id then
        return 0;
      end if;
      v_nickname1:=get_person_nickname(v_actual_user_id);
      v_nickname2:=get_person_nickname(p_person_id);
      v_result:=FAMILYTREE.FIND_PATH(v_nickname1,v_nickname2);
      return length(v_result);
      EXCEPTION
        when no_data_found then
          return 0;
    end getKinshipDegree;
    
    function MaximumKinshipOfAsset(p_my_users_id varchar2,p_asset_id number) return NUMBER as
    v_people varchar2(1000);
    v_max_kinship number:=-1;
    i number:=-1;
    v_count number:=0;
    begin
--    
--    v_people:=TAGS_PERSONS_UTILS.getPeople(p_my_users_id,p_asset_id);
--    i:=instr(v_people,'SMRP',1,v_count);
--    while(i<>0) loop
--    
--    
--    i:=instr(v_people,'SMRP',1,v_count);
--    end loop;
    return 0;
    end MaximumKinshipOfAsset;
    
    function get_all_children(p_root_nickname VARCHAR2, p_user_id VARCHAR2) return varchar2 deterministic as
      v_node TreeTable%rowtype;
      
      cursor children_table is select * from TreeTable where user_id = p_user_id and father_id = get_person_id(p_root_nickname);
      
      v_results children_array := children_array();
      v_array_index number := 1;
      v_result_char varchar2(900) := '';
    begin
      open children_table;
      loop
        fetch children_table into v_node;
        exit when children_table%notfound;
        
        v_results.extend();
        v_results(v_array_index) := v_node;
        
        v_array_index := v_array_index + 1;
      end loop;
      close children_table;
      
      for index_res in v_results.first..v_results.last loop
        v_result_char := v_result_char || ' ' || v_results(index_res).person_id;
      end loop;
      return v_result_char;
    end get_all_children;
    
    procedure update_parent(p_root_id number, p_new_parent_id number) as 
    begin
      update TreeTable
      set father_id = p_new_parent_id where person_id = p_root_id;
    end update_parent;

    PROCEDURE add_person(p_user_id VARCHAR2,p_nickname VARCHAR2,
              p_relation_path VARCHAR2, p_root_nickname VARCHAR2, p_update NUMBER)
              AS
      v_existing_root NUMBER;
      v_current_node TreeTable%ROWTYPE;
      v_newPerson_id TreeTable.person_id%TYPE;
      
      v_last_type VARCHAR(10) := 'X';
      v_before_last_type VARCHAR(10) := 'X';
      
      v_previous_was_son BOOLEAN := false;
      v_previous_son TreeTable.person_id%TYPE;
      
      v_update_number NUMBER;
      BEGIN
        --Check for the existence of the root person
        SELECT COUNT(1) INTO v_existing_root FROM TreeTable WHERE p_root_nickname = nickname;
        IF (v_existing_root = 0) THEN
          RAISE AppExceptions.invalid_nickname;
        END IF;
  
        SELECT * INTO v_current_node FROM TreeTable WHERE p_root_nickname = nickname;

        --Traverse the tree from the selected root following the given path until the penultimate node      
        FOR contor IN 1..LENGTH(p_relation_path) - 1 LOOP
          IF(SUBSTR(p_relation_path, contor, 1) = 'F') THEN
            v_before_last_type := v_last_type;
            v_last_type := 'F';
            SELECT * INTO v_current_node FROM TreeTable WHERE person_id = v_current_node.father_id;
            v_previous_was_son := false;
          ELSE IF(SUBSTR(p_relation_path, contor, 1) = 'S') THEN
                  IF(v_before_last_type = 'S' and v_last_type = 'F') THEN
                    SELECT * INTO v_current_node FROM (SELECT * FROM TreeTable WHERE father_id = v_current_node.person_id AND person_id != v_previous_son) WHERE ROWNUM = 1;
                  ELSE
                    SELECT * INTO v_current_node FROM (SELECT * FROM TreeTable WHERE father_id = v_current_node.person_id) WHERE ROWNUM = 1;
                  END IF;
                  v_previous_son := v_current_node.person_id;
                  v_before_last_type := v_last_type;
                  v_last_type := 'S';
               END IF;
          END IF;
        END LOOP;
        --separate cases of update and create throwing corresponding exceptions
        CASE 
        WHEN p_update = 1 THEN 
          IF(SUBSTR(p_relation_path, length(p_relation_path)) = 'N') THEN
            raise AppExceptions.invalid_input_type;
          ELSE
            IF(SUBSTR(p_relation_path, length(p_relation_path)) = 'S') THEN
              SELECT * INTO v_current_node FROM (SELECT * FROM TreeTable WHERE father_id = v_current_node.person_id) WHERE ROWNUM = 1;
              SELECT COUNT(1) INTO v_update_number FROM TreeTable WHERE nickname = p_nickname;
              IF(v_update_number = 0) THEN
                UPDATE TreeTable SET Nickname = p_nickname WHERE person_id = v_current_node.person_id;
              ELSE
                raise AppExceptions.invalid_nickname;
              END IF;
            ELSIF(SUBSTR(p_relation_path, length(p_relation_path)) = 'F') THEN 
              SELECT * INTO v_current_node FROM TreeTable WHERE person_id = v_current_node.father_id;
              SELECT COUNT(1) INTO v_update_number FROM TreeTable WHERE nickname = p_nickname;
              IF(v_update_number = 0) THEN
                UPDATE TreeTable SET Nickname = p_nickname WHERE person_id = v_current_node.person_id;
              ELSE
                raise AppExceptions.invalid_nickname;
              END IF;
            ELSE
              raise AppExceptions.invalid_input_type;
            END IF;   
          END IF;
        WHEN p_update = 0 THEN
          IF(SUBSTR(p_relation_path, length(p_relation_path)) = 'N') THEN
            SELECT MAX(Person_id) INTO v_newPerson_id FROM TreeTable;
            v_newPerson_id := v_newPerson_id + 1;
            SELECT COUNT(1) INTO v_update_number FROM TreeTable WHERE nickname = p_nickname;
            IF(v_update_number = 0) THEN
               INSERT INTO TreeTable VALUES (v_newPerson_id, p_user_id, p_nickname, v_current_node.person_id);
            ELSE
              raise AppExceptions.invalid_nickname;
            END IF;
          ELSE 
            raise AppExceptions.invalid_input_type;
          END IF;
        ELSE raise AppExceptions.invalid_input_type;  
        END CASE;
   
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
           RAISE AppExceptions.invalid_input_type;        
      END add_person;
          
END FamilyTree;
