SET SERVEROUTPUT ON;

CREATE TABLE stored_metrics_related_info(
registry_date DATE,
birthday DATE,
telephone CHAR(10),
nr_videos NUMBER(10,0)
);
/

drop materialized view user_metrics_related_info;
--drop table stored_metrics_related_info;

CREATE MATERIALIZED VIEW user_metrics_related_info
refresh complete start with (sysdate) next (sysdate+1/1440)
AS SELECT birthday,telephone,count(videos.videos_id) AS nr_videos from my_users full outer join videos on my_users.my_users_id=videos.my_users_id group by my_users.my_users_id, telephone, my_users.birthday, birthday;
/

--EXEC dbms_stats.gather_schema_stats('student', cascade=>TRUE);
--select birthday,telephone,count(videos.videos_id) from my_users join videos on my_users.my_users_id=videos.my_users_id group by my_users.my_users_id,telephone,my_users.birthday;

CREATE OR REPLACE TRIGGER save_metrics_trigger
AFTER INSERT OR UPDATE OR DELETE ON user_metrics_related_info
begin 
  insert into stored_metrics_related_info(registry_date, birthday, telephone, nr_videos) 
  select (select sysdate FROM DUAL), birthday, telephone, nr_videos from user_metrics_related_info;
end;
/

CREATE OR REPLACE FUNCTION getAverageAgeStatistic(p_inputDate DATE) RETURN NUMBER DETERMINISTIC AS
    v_age NUMBER;
BEGIN
    SELECT AVG(TRUNC((SYSDATE - BIRTHDAY)/ 365.25)) INTO v_age FROM STORED_METRICS_RELATED_INFO WHERE REGISTRY_DATE = p_inputDate;
    RETURN v_age;
END getAverageAgeStatistic;
/

CREATE OR REPLACE FUNCTION getAverageAgeMaterial RETURN NUMBER DETERMINISTIC AS
    v_age NUMBER;
BEGIN
    SELECT AVG(TRUNC((SYSDATE - BIRTHDAY)/ 365.25)) INTO v_age FROM USER_METRICS_RELATED_INFO;
    RETURN v_age;
END getAverageAgeMaterial;
/

CREATE OR REPLACE FUNCTION getTelephoneStatistic(p_inputDate DATE) RETURN VARCHAR2 DETERMINISTIC AS
    v_orange NUMBER;
    v_vodafone NUMBER;
    v_digi NUMBER;
    v_result VARCHAR2(255 BYTE);
BEGIN
    SELECT COUNT(TELEPHONE) INTO v_orange FROM STORED_METRICS_RELATED_INFO WHERE SUBSTR(TELEPHONE,3,1) = '4';
    SELECT COUNT(TELEPHONE) INTO v_vodafone FROM STORED_METRICS_RELATED_INFO WHERE SUBSTR(TELEPHONE,3,1) = '2';
    SELECT COUNT(TELEPHONE) INTO v_digi FROM STORED_METRICS_RELATED_INFO WHERE SUBSTR(TELEPHONE,3,1) = '5';
    v_result := 'Numar utilizatori Orange:' || v_orange || ' Numar utilizatori Vodafone:'  || v_vodafone || ' Numar utilizatori Digi:'  || v_digi;
    RETURN v_result;
END getTelephoneStatistic;
/

CREATE OR REPLACE FUNCTION getTelephoneMaterial RETURN VARCHAR2 DETERMINISTIC AS
    v_orange NUMBER;
    v_vodafone NUMBER;
    v_digi NUMBER;
    v_result VARCHAR2(255 BYTE);
BEGIN
    SELECT COUNT(TELEPHONE) INTO v_orange FROM USER_METRICS_RELATED_INFO WHERE SUBSTR(TELEPHONE,3,1) = '4';
    SELECT COUNT(TELEPHONE) INTO v_vodafone FROM USER_METRICS_RELATED_INFO WHERE SUBSTR(TELEPHONE,3,1) = '2';
    SELECT COUNT(TELEPHONE) INTO v_digi FROM USER_METRICS_RELATED_INFO WHERE SUBSTR(TELEPHONE,3,1) = '5';
    v_result := 'Numar utilizatori Orange:' || v_orange || ' Numar utilizatori Vodafone:'  || v_vodafone || ' Numar utilizatori Digi:'  || v_digi;
    RETURN v_result;
END getTelephoneMaterial;
/


CREATE OR REPLACE FUNCTION getAverageVideosStat(p_inputDate DATE) RETURN NUMBER DETERMINISTIC AS
    v_result NUMBER;
BEGIN
    SELECT AVG(TRUNC(NR_Videos)) INTO v_result FROM STORED_METRICS_RELATED_INFO WHERE REGISTRY_DATE = p_inputDate;
    RETURN v_result;
END getAverageVideosStat;
/

CREATE OR REPLACE FUNCTION getAverageVideosMat RETURN NUMBER DETERMINISTIC AS
    v_result NUMBER;
BEGIN
    SELECT AVG(TRUNC(NR_Videos)) INTO v_result FROM USER_METRICS_RELATED_INFO;
    RETURN v_result;
END getAverageVideosMat;
/

DECLARE
    v_test NUMBER;
    v_test2 VARCHAR2(255 BYTE);
BEGIN
    v_test := getAverageAgeStatistic(TO_DATE('29/05/2017 22:02:31', 'DD/MM/YYYY HH24:MI:SS'));
    DBMS_OUTPUT.PUT_LINE(v_test);
    v_test := getAverageAgeMaterial();
    DBMS_OUTPUT.PUT_LINE(v_test);
    v_test2 := getTelephoneStatistic(TO_DATE('29/05/2017 22:02:31', 'DD/MM/YYYY HH24:MI:SS'));
    DBMS_OUTPUT.PUT_LINE(v_test2);
    v_test2 := getTelephoneMaterial();
    DBMS_OUTPUT.PUT_LINE(v_test2);
    v_test := getAverageVideosStat(TO_DATE('29/05/2017 22:02:31', 'DD/MM/YYYY HH24:MI:SS'));
    DBMS_OUTPUT.PUT_LINE(v_test);
    v_test := getAverageVideosMat();
    DBMS_OUTPUT.PUT_LINE(v_test);
END;
