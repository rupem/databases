DROP TABLE TreeTable;
CREATE TABLE TreeTable(Person_ID NUMBER(10,0) NOT NULL, User_ID NUMBER(10,0) NOT NULL,
        Nickname VARCHAR2(255 BYTE), Father_ID NUMBER(10,0), PRIMARY KEY(Person_ID), constraint nickname_per_user unique(Nickname, User_ID));
/
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (1,1,'Taticut',0);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (2,1,'actualUser1',1);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (3,1,'Copilas2',1);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (4,1,'Copilas1DeActualUser',2);
INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (5,1,'Copilas1DeCopilas2',3);
/
set serveroutput on;
declare
v_contor integer;
v_nume_persoana varchar2(100);
v_id_person number(10,0);
v_no_persons number(10,0);
v_nr number(10,0):=6;
v_rand1 number;
begin
  for v_contor in 1..1000000 loop
    select count(*) into v_no_persons from treetable;
    v_id_person := TRUNC(DBMS_RANDOM.VALUE(1, v_no_persons));
    
    v_rand1 := TRUNC(DBMS_RANDOM.VALUE(1, 85));
    --v_nume_persoana:=substr(v_tags,instr(v_tags,'#',1,v_rand1)+1,instr(v_tags,'#',1,v_rand1+1)-instr(v_tags,'#',1,v_rand1)-1);
    INSERT INTO TreeTable(Person_ID, User_ID, Nickname, Father_ID) VALUES (v_nr ,1,'a'||v_nr,v_id_person);
    v_nr:=v_nr+1;
    --DBMS_OUTPUT.PUT_LINE(v_nume_tag);
  end loop;
  --select * from treetable

end;

select * from treetable;
