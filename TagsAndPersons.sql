set serveroutput on;
CREATE OR REPLACE PACKAGE tags_persons_utils AS 
   FUNCTION getTags (p_my_users_id VARCHAR2, asset_id NUMBER, 
      file_type varchar2) 
      RETURN varchar2;
   FUNCTION getPeople(p_my_users_id VARCHAR2, asset_id NUMBER, 
      file_type varchar2) 
      RETURN varchar2; 
      
end tags_persons_utils;
/
CREATE OR REPLACE PACKAGE BODY tags_persons_utils AS
FUNCTION getTags (p_my_users_id VARCHAR2, asset_id NUMBER, 
      file_type varchar2) 
      RETURN varchar2 as
      TYPE cursor_type IS REF CURSOR;
      v_cursor cursor_type;
      v_statement varchar2(200);
      v_tag_name varchar2(100);
      v_table_initial varchar2(2):=substr(file_type,0,1);
      v_foreign_table_name varchar2(100):=file_type||'_TAGS';
      v_table_name varchar2(100):=file_type||'s';
      v_result varchar2(1000):='';
BEGIN
  
  v_statement:='select t.name from '||v_table_name||' '||v_table_initial||' join '||v_foreign_table_name||' atg on atg.'||v_table_name||'_ID='
            ||v_table_initial||'.'||v_table_name||'_ID join tags t on atg.TAGS_ID=t.TAGS_ID WHERE '||v_table_initial||'.'||v_table_name||'_ID='||asset_id;
  SYS.DBMS_OUTPUT.PUT_LINE(v_statement);
  OPEN v_cursor FOR v_statement;
  LOOP
  FETCH v_cursor INTO v_tag_name; 
  EXIT WHEN v_cursor%NOTFOUND; 
  
  SYS.DBMS_OUTPUT.put_line(v_tag_name);
    v_result:=v_result||upper(v_tag_name)||'SMRP';
  END LOOP; 
  CLOSE v_cursor;
  SYS.DBMS_OUTPUT.put_line(v_result);
  return v_result;
END getTags;

FUNCTION getPeople (p_my_users_id VARCHAR2, asset_id NUMBER, 
      file_type varchar2) 
      RETURN varchar2 as
          TYPE cursor_type IS REF CURSOR;
      v_cursor cursor_type;
      v_statement varchar2(200);
      v_tag_name varchar2(100);
      v_table_initial varchar2(2):=substr(file_type,0,1);
      v_foreign_table_name varchar2(100):=file_type||'_PEOPLE';
      v_table_name varchar2(100):=file_type||'s';
      v_result varchar2(1000):='';
BEGIN
  
  v_statement:='select t.nickname from '||v_table_name||' '||v_table_initial||' join '||v_foreign_table_name||' atg on atg.'||v_table_name||'_ID='
            ||v_table_initial||'.'||v_table_name||'_ID join people t on atg.PEOPLE_ID=t.PEOPLE_ID WHERE '||v_table_initial||'.'||v_table_name||'_ID='||asset_id;
  SYS.DBMS_OUTPUT.PUT_LINE(v_statement);
  OPEN v_cursor FOR v_statement;
  LOOP
  FETCH v_cursor INTO v_tag_name; 
  EXIT WHEN v_cursor%NOTFOUND; 
  
  SYS.DBMS_OUTPUT.put_line(v_tag_name);
    v_result:=v_result||upper(v_tag_name)||'SMRP';
  END LOOP; 
  CLOSE v_cursor;
  SYS.DBMS_OUTPUT.put_line(v_result);
  return v_result;
      end getPeople;
END tags_persons_utils;


