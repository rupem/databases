DROP SEQUENCE people_id_seq;
/
CREATE SEQUENCE people_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER people_id
BEFORE INSERT ON people
FOR EACH ROW
DECLARE
BEGIN
  :NEW.people_id:=people_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE docpeople_id_seq;
/
CREATE SEQUENCE docpeople_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER docpeople_id
BEFORE INSERT ON doc_people
FOR EACH ROW
DECLARE
BEGIN
  :NEW.doc_people_id:=docpeople_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE pdfpeople_id_seq;
/
CREATE SEQUENCE pdfpeople_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER pdfpeople_id
BEFORE INSERT ON pdf_people
FOR EACH ROW
DECLARE
BEGIN
  :NEW.pdf_people_id:=pdfpeople_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE videopeople_id_seq;
/
CREATE SEQUENCE videopeople_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER videopeople_id
BEFORE INSERT ON video_people
FOR EACH ROW
DECLARE
BEGIN
  :NEW.video_people_id:=videopeople_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE photopeople_id_seq;
/
CREATE SEQUENCE photopeople_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER photopeople_id
BEFORE INSERT ON photo_people
FOR EACH ROW
DECLARE
BEGIN
  :NEW.photo_people_id:=photopeople_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE photo_tags_id_seq;
/
CREATE SEQUENCE photo_tags_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER photo_tags_id
BEFORE INSERT ON photo_tags
FOR EACH ROW
DECLARE
BEGIN
  :NEW.photo_tags_id:=photo_tags_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE pdf_tags_id_seq;
/
CREATE SEQUENCE pdf_tags_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER pdf_tags_id
BEFORE INSERT ON pdf_tags
FOR EACH ROW
DECLARE
BEGIN
  :NEW.pdf_tags_id:=pdf_tags_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE video_tags_id_seq;
/
CREATE SEQUENCE video_tags_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER video_tags_id
BEFORE INSERT ON video_tags
FOR EACH ROW
DECLARE
BEGIN
  :NEW.video_tags_id:=video_tags_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE pdf_tags_id_seq;
/
CREATE SEQUENCE pdf_tags_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER doc_tags_id
BEFORE INSERT ON doc_tags
FOR EACH ROW
DECLARE
BEGIN
  :NEW.doc_tags_id:=doc_tags_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE tags_id_seq;
/
CREATE SEQUENCE tags_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER tags_id
BEFORE INSERT ON tags
FOR EACH ROW
DECLARE
tg_id number(10,0);
nr_tg number(10,0);
BEGIN
  :NEW.tags_id:=tags_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE docs_id_seq;
/
CREATE SEQUENCE docs_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER docs_id
BEFORE INSERT ON DOCS
FOR EACH ROW
BEGIN
  :NEW.docs_id := docs_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE pdfs_id_seq;
/
CREATE SEQUENCE pdfs_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER pdfs_id
BEFORE INSERT ON PDFS
FOR EACH ROW
BEGIN
  :NEW.pdfs_id := pdfs_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE videos_id_seq;
/
CREATE SEQUENCE videos_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER videos_id
BEFORE INSERT ON VIDEOS
FOR EACH ROW
BEGIN
  :NEW.VIDEOS_id := videos_id_seq.NEXTVAL;
END;
/
DROP SEQUENCE photos_id_seq;
/
CREATE SEQUENCE photos_id_seq 
START WITH 1 INCREMENT BY 1;
/
CREATE OR REPLACE TRIGGER photos_id
BEFORE INSERT ON PHOTOS
FOR EACH ROW
BEGIN
  :NEW.photos_id := photos_id_seq.NEXTVAL;
END;