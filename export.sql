--create or replace directory DB_DIR as 'P:\II\SGBD\Labs\10';
--grant read, write on directory DB_DIR to PUBLIC;
--GRANT EXECUTE ON SYS.utl_file TO studenta;
set serveroutput on;
DECLARE
  db_file UTL_FILE.FILE_TYPE;
  TYPE cursorType IS REF CURSOR;
  constraints_cursor cursorType;
  CURSOR tabless IS SELECT object_name
     FROM ALL_OBJECTS 
     WHERE object_type = 'TABLE' AND owner='STUDENTA' order by created;   
  CURSOR functionss IS SELECT DBMS_METADATA.GET_DDL('FUNCTION', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'FUNCTION';
  CURSOR procedures IS SELECT DBMS_METADATA.GET_DDL('PROCEDURE', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'PROCEDURE';
  CURSOR viewss IS SELECT DBMS_METADATA.GET_DDL('VIEW', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'VIEW';
  CURSOR typess IS SELECT DBMS_METADATA.GET_DDL('TYPE', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'TYPE';  
  CURSOR sequences IS SELECT object_name
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'SEQUENCE';
  CURSOR triggerss IS SELECT DBMS_METADATA.GET_DDL('TRIGGER', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'TRIGGER';
  CURSOR packagess IS SELECT DBMS_METADATA.GET_DDL('PACKAGE', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'PACKAGE';
  CURSOR packages_body IS SELECT DBMS_METADATA.GET_DDL('PACKAGE_BODY', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'PACKAGE BODY';
  CURSOR indexess IS SELECT DBMS_METADATA.GET_DDL('INDEX', object_name)
    FROM ALL_OBJECTS WHERE OWNER='STUDENTA'  
    AND object_type = 'INDEX';    
  v_ddl CLOB;
  v_name VARCHAR2(255);
  v_stmt VARCHAR2(255);
  v_insert VARCHAR2(32767);
  v_cursor INTEGER;
  v_status INTEGER;
  
  v_column VARCHAR2(4000);
  v_descTable DBMS_SQL.DESC_TAB;
  v_count NUMBER;
  v_user VARCHAR(200);
BEGIN
  db_file := UTL_FILE.FOPEN('DB_DIR', 'database_content.sql','W', 32767);
  
  EXECUTE IMMEDIATE 'SELECT user from dual' INTO v_user;
  
  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM, 'CONSTRAINTS',false);
  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM, 'REF_CONSTRAINTS',false);
  
  OPEN tabless;
  LOOP
    FETCH tabless INTO v_name;
    EXIT WHEN tabless%NOTFOUND;
    v_ddl := 'DROP TABLE ' || v_name || ';';
    
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    
    v_ddl := DBMS_METADATA.GET_DDL('TABLE',v_name);
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    --CREATE TABLE
    v_ddl := v_ddl || ';';
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    
  END LOOP;
  CLOSE TABLESS;
  
  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'DEFAULT');

  OPEN tabless;
  LOOP
    FETCH tabless INTO v_name;
    EXIT WHEN tabless%NOTFOUND;
    --prepare INSERT
    v_stmt := 'SELECT * FROM ' || v_name;
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    DBMS_SQL.PARSE(v_cursor, v_stmt, DBMS_SQL.NATIVE);
    DBMS_SQL.DESCRIBE_COLUMNS(v_cursor, v_count, v_descTable);
    FOR ind IN 1 .. v_count LOOP
      DBMS_SQL.DEFINE_COLUMN(v_cursor, ind, v_column, 4000);
    END LOOP;
    v_status := DBMS_SQL.EXECUTE(v_cursor);
    --insert each row
    LOOP
    EXIT WHEN DBMS_SQL.FETCH_ROWS(v_cursor) = 0; 
      v_insert := 'INSERT INTO ' ||v_name || ' VALUES(';
      FOR ind IN 1 .. v_count LOOP
        DBMS_SQL.COLUMN_VALUE(v_cursor, ind, v_column);
        IF(v_descTable(ind).col_type = 1 OR --varcahr2
          v_descTable(ind).col_type = 112 OR --clob type
          v_descTable(ind).col_type = 180 OR  --timestampt
          v_descTable(ind).col_type = 12) THEN --date
          v_column := REPLACE(v_column, '''', '''''');
          v_column := REPLACE(v_column, '"', '""');
          v_column := REPLACE(v_column, '\', '\\');
          v_insert := v_insert || '''' || v_column || ''', ';
        ELSE
          
          v_insert := v_insert || v_column || ', ';
        END IF;
      END LOOP;
      v_insert := SUBSTR(v_insert, 1, length(v_insert)-2);
      v_insert := v_insert || ');';
      UTL_FILE.PUT_LINE(db_file, v_insert);
    END LOOP;
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    
  END LOOP;
  CLOSE TABLESS;

  OPEN functionss;
  LOOP
    FETCH functionss INTO v_ddl;
    EXIT WHEN functionss%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');   
  END LOOP;
  CLOSE functionss;
  
  OPEN procedures;
  LOOP
    FETCH procedures INTO v_ddl;
    EXIT WHEN procedures%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');   
  END LOOP;
  CLOSE procedures;
  
  OPEN viewss;
  LOOP
    FETCH viewss INTO v_ddl;
    EXIT WHEN viewss%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');   
  END LOOP;
  CLOSE viewss;
    
      
  OPEN typess;
  LOOP
    FETCH typess INTO v_ddl;
    EXIT WHEN typess%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');   
  END LOOP;
  CLOSE typess;  
    
  OPEN sequences;
  LOOP
    FETCH sequences INTO v_name;
    EXIT WHEN sequences%NOTFOUND;
    v_ddl := 'DROP SEQUENCE ' || v_name;
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');    
    
    v_ddl := DBMS_METADATA.GET_DDL('SEQUENCE', v_name);
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');   
  END LOOP;
  CLOSE sequences;
  
  OPEN triggerss;
  LOOP
    FETCH triggerss INTO v_ddl;
    EXIT WHEN triggerss%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');    
  END LOOP;
  CLOSE triggerss;
  
  OPEN packagess;
    UTL_FILE.PUT_LINE(db_file, '/');
  LOOP
    FETCH packagess INTO v_ddl;
    EXIT WHEN packagess%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    IF(instr(v_ddl, 'CREATE OR REPLACE PACKAGE BODY') <> 0) THEN
      v_ddl := substr(v_ddl, 0, instr(v_ddl,'CREATE OR REPLACE PACKAGE BODY') - 1);
    END IF;
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');
  END LOOP;
  CLOSE packagess;
  
  OPEN packages_body;
  LOOP
    FETCH packages_body INTO v_ddl;
    EXIT WHEN packages_body%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');
  END LOOP;
  CLOSE packages_body;
  
  OPEN indexess;
  LOOP
    FETCH indexess INTO v_ddl;
    EXIT WHEN indexess%NOTFOUND;
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');   
  END LOOP;
  CLOSE indexess;
  
  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM, 'CONSTRAINTS_AS_ALTER', true);
  
  OPEN tabless;
  LOOP
    FETCH tabless INTO v_name;
    EXIT WHEN tabless%NOTFOUND;
    v_ddl := DBMS_METADATA.GET_DDL('TABLE',v_name);
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    IF(instr(v_ddl, 'ALTER') <> 0) THEN
      v_ddl := SUBSTR(v_ddl,instr(v_ddl,'ALTER')); 
      UTL_FILE.PUT_LINE(db_file, v_ddl);
      UTL_FILE.PUT_LINE(db_file, '/');  
    END IF;
  END LOOP;
  CLOSE TABLESS;

  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'DEFAULT');

  UTL_FILE.FCLOSE(db_file);
 END;
 /
 