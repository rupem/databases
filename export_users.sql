--create or replace directory DB_DIR as 'P:\II\SGBD\Labs\10';
--grant read, write on directory DB_DIR to PUBLIC;
--GRANT EXECUTE ON SYS.utl_file TO student;
set serveroutput on;
CREATE OR REPLACE PROCEDURE export_users AS
  db_file UTL_FILE.FILE_TYPE;
  TYPE cursorType IS REF CURSOR;
  constraints_cursor cursorType;     
  v_ddl CLOB;
  v_name VARCHAR2(255) := 'USERS';
  
  v_stmt VARCHAR2(255);
  v_insert VARCHAR2(32767);
  v_cursor INTEGER;
  v_status INTEGER;
  
  v_column VARCHAR2(4000);
  v_descTable DBMS_SQL.DESC_TAB;
  v_count NUMBER;
  v_user VARCHAR(200) := 'USERS';
  
BEGIN
  db_file := UTL_FILE.FOPEN('DB_DIR', 'database_users.sql','W', 32767);
  EXECUTE IMMEDIATE 'SELECT user from dual' INTO v_user;
    v_ddl := 'DROP TABLE ' || v_name;
    
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');
    
    v_ddl := DBMS_METADATA.GET_DDL('TABLE',v_name);
    v_ddl := replace(v_ddl, '"' || v_user|| '".','');
    --CREATE TABLE
    
    UTL_FILE.PUT_LINE(db_file, v_ddl);
    UTL_FILE.PUT_LINE(db_file, '/');
    
    --prepare INSERT
    v_stmt := 'SELECT * FROM ' || v_name;
    v_cursor := DBMS_SQL.OPEN_CURSOR;
    DBMS_SQL.PARSE(v_cursor, v_stmt, DBMS_SQL.NATIVE);
    DBMS_SQL.DESCRIBE_COLUMNS(v_cursor, v_count, v_descTable);
    FOR ind IN 1 .. v_count LOOP
      DBMS_SQL.DEFINE_COLUMN(v_cursor, ind, v_column, 4000);
    END LOOP;
    v_status := DBMS_SQL.EXECUTE(v_cursor);
    --insert each row
    LOOP
    EXIT WHEN DBMS_SQL.FETCH_ROWS(v_cursor) = 0; 
      v_insert := 'INSERT INTO ' ||v_name || ' VALUES(';
      FOR ind IN 1 .. v_count LOOP
        DBMS_SQL.COLUMN_VALUE(v_cursor, ind, v_column);
        IF(v_descTable(ind).col_type = 1 OR --varcahr2
          v_descTable(ind).col_type = 112 OR --clob type
          v_descTable(ind).col_type = 180 OR  --timestampt
          v_descTable(ind).col_type = 12) THEN --date
          v_column := REPLACE(v_column, '''', '''''');
          v_column := REPLACE(v_column, '"', '""');
          v_column := REPLACE(v_column, '\', '\\');
          v_insert := v_insert || '''' || v_column || ''', ';
        ELSE
          
          v_insert := v_insert || v_column || ', ';
        END IF;
      END LOOP;
      v_insert := SUBSTR(v_insert, 1, length(v_insert)-2);
      v_insert := v_insert || ');';
      UTL_FILE.PUT_LINE(db_file, v_insert);
      UTL_FILE.PUT_LINE(db_file, '/');
    END LOOP;
    DBMS_SQL.CLOSE_CURSOR(v_cursor);
    

  UTL_FILE.FCLOSE(db_file);
END;

/
BEGIN
  export_users();
END;