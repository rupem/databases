create index pid_fid on treetable(person_id,father_id);
create index nick on treetable(nickname);
create index u_id on treetable(user_id);
create index getdegree on treetable(familytree.getkinshipdegree(user_id,person_id));
