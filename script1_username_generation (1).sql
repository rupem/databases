SET SERVEROUTPUT ON;
DROP TABLE MY_USERS;

--DROP TRIGGER MY_USERS_SOFT_DELETE;
--DELETE FROM MY_USERS;
CREATE TABLE MY_USERS(MY_USERS_ID NUMBER(10,0) NOT NULL, FIRST_NAME VARCHAR2(255),
        LAST_NAME VARCHAR2(255), BIRTHDAY DATE, TELEPHONE CHAR(10) UNIQUE,

        EMAIL VARCHAR2(255) UNIQUE, USERNAME VARCHAR2(255), HASH VARCHAR2(255), 
        IS_DELETED NUMBER(1) DEFAULT 0, PRIMARY KEY(USER_ID));
   

DECLARE
    CURSOR users_list IS SELECT ID, NAME, USERNAME FROM USERS;
    v_nr_tel number;
    v_linie users_list%ROWTYPE;
    v_nume USERS.NAME%TYPE;
    v_prenume1 USERS.NAME%TYPE;
    v_prenume2 USERS.NAME%TYPE;
    v_prenume USERS.NAME%TYPE;
    v_username USERS.NAME%TYPE;
    v_rand1 NUMBER := 0;
    v_rand2 NUMBER := 0;
    v_rand3 NUMBER := 0;
    v_tel CHAR(10);
    v_doubleN NUMBER;
    v_contor INTEGER := 0;
    v_checkLast1 VARCHAR2(2);
    v_checkLast2 VARCHAR2(2);
    v_email VARCHAR2(50);
    v_date DATE;
    l_hash VARCHAR2(255);
BEGIN
    FOR v_contor IN 1..10000 LOOP
        v_doubleN := TRUNC(DBMS_RANDOM.VALUE(0, 2));
        WHILE (v_rand1 = v_rand2 OR v_rand1 = v_rand3 OR v_rand2 = v_rand3) LOOP
            v_rand1 := TRUNC(DBMS_RANDOM.VALUE(1, 500));
            v_rand2 := TRUNC(DBMS_RANDOM.VALUE(1, 500));
            v_rand3 := TRUNC(DBMS_RANDOM.VALUE(1, 500));
        END LOOP;
        -- Verificari si generari de nume/prenume
        SELECT (NAME) INTO v_nume FROM (SELECT ROWNUM as "myNR", NAME FROM USERS) WHERE "myNR" = v_rand1;
        v_nume := LTRIM(SUBSTR(v_nume, INSTR(v_nume,' ')));
        SELECT (NAME) INTO v_prenume1 FROM (SELECT ROWNUM as "myNR", NAME FROM USERS) WHERE "myNR" = v_rand2;
        v_prenume1 := LTRIM(SUBSTR(v_prenume1, 0, INSTR(v_prenume1,' ')));
        IF v_doubleN = 1 THEN 
            SELECT (NAME) INTO v_prenume2 FROM (SELECT ROWNUM as "myNR", NAME FROM USERS) WHERE "myNR" = v_rand3;
            v_prenume2 := LTRIM(SUBSTR(v_prenume2, 0, INSTR(v_prenume2,' ')));
            v_checkLast1 := SUBSTR(v_prenume1, LENGTH(v_prenume1)-1,1);
            v_checkLast2 := SUBSTR(v_prenume2, LENGTH(v_prenume2)-1,1);
            if ((v_checkLast1 = 'a' AND v_checkLast2 != 'a') OR (v_checkLast1 != 'a' AND v_checkLast2 = 'a')
                OR (v_prenume1 = v_prenume2)) THEN
                WHILE (((v_checkLast1 = 'a' AND v_checkLast2 != 'a') OR (v_checkLast1 != 'a' AND v_checkLast2 = 'a')
                    OR (v_prenume1 = v_prenume2))) LOOP
                    v_rand2 := TRUNC(DBMS_RANDOM.VALUE(1, 500));
                    v_rand3 := TRUNC(DBMS_RANDOM.VALUE(1, 500));
                    SELECT (NAME) INTO v_prenume1 FROM (SELECT ROWNUM as "myNR", NAME FROM USERS) WHERE "myNR" = v_rand2;
                    v_prenume1 := LTRIM(SUBSTR(v_prenume1, 0, INSTR(v_prenume1,' ')));
                    SELECT (NAME) INTO v_prenume2 FROM (SELECT ROWNUM as "myNR", NAME FROM USERS) WHERE "myNR" = v_rand3;
                    v_prenume2 := LTRIM(SUBSTR(v_prenume2, 0, INSTR(v_prenume2,' ')));
                    v_checkLast1 := SUBSTR(v_prenume1, LENGTH(v_prenume1)-1,1);
                    v_checkLast2 := SUBSTR(v_prenume2, LENGTH(v_prenume2)-1,1);
                END LOOP;
            END IF;
        ELSE v_prenume2 := '';
        END IF;
        v_prenume := RTRIM(LTRIM(v_prenume1))||' '||RTRIM(LTRIM(v_prenume2));
        -- Generare data de nastere
        v_rand1 := TRUNC(DBMS_RANDOM.VALUE(1, 28));
        v_rand2 := TRUNC(DBMS_RANDOM.VALUE(1, 12));
        v_date := TO_DATE(v_rand1||'/'||v_rand2||'/'||'1997', 'dd/mm/yyyy');
        -- Generare nr de telefon
        
        loop
        v_rand1 := TRUNC(DBMS_RANDOM.VALUE(1, 4));
        v_nr_tel:=TRUNC(DBMS_RANDOM.VALUE(100000, 999999));
        /*
        IF v_rand1 = 1 THEN
            v_tel := '0740' || to_char(MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')+100000,1000000));
            ELSE IF v_rand1 = 2 THEN 
                v_tel := '0757' || to_char(MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')+100000,1000000));
                ELSE v_tel := '0723' || to_char(MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')+100000,1000000));
            END IF;
        END IF;
        */
        IF v_rand1 = 1 THEN
            v_tel := '0740' || v_nr_tel;
            ELSE IF v_rand1 = 2 THEN 
                v_tel := '0757' || v_nr_tel;
                ELSE v_tel := '0723' || v_nr_tel;
            END IF;
        END IF;
        select count(*) into v_rand1 from my_users where telephone=v_tel;
        exit when v_rand1=0;
        end loop;
        -- Generare email + username
        
        v_rand1 := TRUNC(DBMS_RANDOM.VALUE(1, 4));
        IF v_rand1 = 1 THEN
            v_email := LOWER(v_nume) || '_' || LTRIM(v_prenume1) || '@gmail.com';
            v_username := LOWER(v_nume) || '_' || LTRIM(v_prenume1) || MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'),100);
            ELSE IF v_rand1 = 2 THEN 
                v_email := LOWER(v_nume) || '.' || LOWER(LTRIM(v_prenume1)) || '@hotmail.com';
                v_username := LOWER(v_nume) || '.' || LOWER(LTRIM(v_prenume1)) || MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'),100);
                ELSE 
                    v_email := v_nume || LOWER(LTRIM(v_prenume1)) || '@yahoo.com';
                    v_username := v_nume || LOWER(LTRIM(v_prenume1)) || MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'),100);
            END IF;
        END IF;
        v_email := REPLACE(v_email, ' ','');
        
        v_username := REPLACE(v_username, ' ','');
        LOOP
        SELECT COUNT (MY_USERS_ID) INTO v_rand1 FROM MY_USERS WHERE EMAIL = v_email;
        EXIT WHEN v_rand1=0;
        IF v_rand1 != 0 THEN
            v_email := v_email || MOD(TO_NUMBER(SYS_GUID(),'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'),100000000);
            v_email := REPLACE(v_email, ' ','');
        END IF;
        END LOOP;
        
        l_hash := DBMS_OBFUSCATION_TOOLKIT.md5 (input_string => v_username);
        l_hash := RAWTOHEX (UTL_RAW.cast_to_raw (l_hash));
        --v_username:=concat(concat(v_nume,v_prenume),l_hash);
        loop
        v_rand2:=TRUNC(DBMS_RANDOM.VALUE(1000, 9999));
        v_username:=concat(v_prenume,v_rand2);
        v_username:=REPLACE(v_username, ' ','');
        select count(*) into v_rand1 from my_users where username=v_username;
        exit when v_rand1=0;
        end loop;
        v_prenume := ltrim(rtrim(v_prenume));
        INSERT INTO MY_USERS VALUES(v_contor, v_prenume, v_nume, v_date, v_tel, v_email, v_username, l_hash, 0);
        v_rand1 := 0;
        v_rand2 := 0;
        v_rand3 := 0;
    END LOOP; 
    
END;
